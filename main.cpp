#include <iostream>
#include <stdlib.h>
#include "Cube.hpp"
#include "CPaint.hpp"

#include "GL/glut.h"

# define MUL_INIT	2.0f
# define MUL		g_mul

# define CASE_INIT	0.05f
# define CASE		g_case_size
# define CASE_RESET	g_case_size = CASE_INIT
# define CASE_INC	g_case_size *= MUL
# define CASE_DEC	g_case_size /= MUL

# define RED (g_color[0])
# define GREEN (g_color[1])
# define BLUE (g_color[2])

# define IS_A_COLOR_TYPE	(g_type >= 0 && g_type <= 2)
# define COLOR_INIT		0.5f
# define COLOR_RESET		(RED = GREEN = BLUE = COLOR_INIT)
# define COLOR_RESET_ONE	(IS_A_COLOR_TYPE ? g_color[g_type] = COLOR_INIT : 0)
# define COLOR_INC		(IS_A_COLOR_TYPE ? g_color[g_type] += COLOR_SPEED * g_dir : 0)
# define COLOR_DEC		(IS_A_COLOR_TYPE ? g_color[g_type] -= COLOR_SPEED * g_dir : 0)
# define COLOR_CHKMAX		(IS_A_COLOR_TYPE ? g_color[g_type] > 1 ? g_color[g_type] = 1 : 0 : 0)
# define COLOR_CHKMIN		(IS_A_COLOR_TYPE ? g_color[g_type] < 0 ? g_color[g_type] = 0 : 0 : 0)

# define COLOR_SPEED_INIT	0.01f
# define COLOR_SPEED		g_color_speed
# define COLOR_SPEED_RESET	g_color_speed = COLOR_SPEED_INIT
# define COLOR_SPEED_INC	g_color_speed *= MUL
# define COLOR_SPEED_DEC	g_color_speed /= MUL

# define ACTION_RESET	(g_type == ColorSpeed ? COLOR_SPEED_RESET :	\
			 (g_type == CaseSize ? CASE_RESET : COLOR_RESET))
# define ACTION_INC	(g_type == ColorSpeed ? COLOR_SPEED_INC :	\
			 (g_type == CaseSize ? CASE_INC : COLOR_INC))
# define ACTION_DEC	(g_type == ColorSpeed ? COLOR_SPEED_DEC :	\
			 (g_type == CaseSize ? CASE_DEC : COLOR_DEC))

float	g_dir = 1;
float	g_mul = MUL_INIT;
float	g_case_size = CASE_INIT;
float	g_color_speed = COLOR_SPEED_INIT;
float	g_position[3] = {0.0, 0.0, -3.0};
float	g_color[3] = {COLOR_INIT, COLOR_INIT, COLOR_INIT};
t_Choix	g_type = ColorSpeed;
int	g_id = 0;

void handleResize(int w, int h) {
  //Tell OpenGL how to convert from coordinates to pixel values
  glViewport(0, 0, w, h);
  glMatrixMode(GL_PROJECTION); //Switch to setting the camera perspective
  //Set the camera perspective
  glLoadIdentity(); //Reset the camera
  gluPerspective(DFIELD,                  //The camera angle
		 (double)w/h, //The width-to-height ratio
		 1.0,                   //The near z clipping coordinate
		 200.0);                //The far z clipping coordinate
}

void reset() {
  glEnable(GL_DEPTH_TEST);
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  glMatrixMode(GL_MODELVIEW); //Switch to the drawing perspective
  glLoadIdentity(); //Reset the drawing perspective
  glutSwapBuffers(); //Send the 3D scene to the screen
}

void handleMotion(int x, int y) {
  float x_, y_;
  x_ = -x + WIDTH / 2 + 12;
  y_ = -y + HEIGTH / 2 - 12;
  x_ /= 250;
  y_ /= 250;
  g_position[0] = -x_;
  g_position[1] = y_;
  drawScene();
}

void handleMouse(int key, int state, int x, int y) {
  // std::cout << "key " << (unsigned int)key << std::endl;
  static int last = -1;
  if (key == GLUT_LEFT_BUTTON and last != 0)
    {
      // g_color[0] = 0.0;
      last = 0;
    }
  else if (key == GLUT_RIGHT_BUTTON && last != 1)
    {
      // g_color[0] = 1.0;
      last = 1;
    }
  handleMotion(x, y);
}

void handleKeypress(unsigned char key, int x, int y) {
  t_Choix const tmp(g_type);
  // std::cout << "key " << (unsigned int)key << std::endl;
  switch (key) {
  case 27:
    glutDestroyWindow(g_id);
    break;
  case 'z':
    g_position[1]+=CASE;
    break;
  case 's':
    g_position[1]-=CASE;
    break;
  case 'd':
    g_position[0]+=CASE;
    break;
  case 'q':
    g_position[0]-=CASE;
    break;
  case 'x':
    reset();
    break;
  case 'r':
    if (IS_A_COLOR_TYPE)
      COLOR_RESET_ONE;
    else
      ACTION_RESET;
    break;
  case 't':
    g_type = ColorRed;
    ACTION_RESET;
    g_type = tmp;
    break;
  case '-':
    ACTION_DEC;
    break;
  case '+':
    ACTION_INC;
    break;
  case '1':
    g_type = ColorSpeed;
    break;
  case '2':
    g_type = CaseSize;
    break;
  case '3':
    std::cout << "direction = " << -g_dir << std::endl;
    g_dir = -g_dir;
    break;
  case '4':
    g_type = ColorRed;
    break;
  case '5':
    g_type = ColorGreen;
    break;
  case '6':
    g_type = ColorBlue;
    break;
  }
}

void handleSpecialKeypress(int key, int x, int y) {
  glColor3f(g_color[0], g_color[1], g_color[2]);
  // std::cout << "skey " << (unsigned int)key << std::endl;
  switch (key) {
  case GLUT_KEY_UP:
    g_position[1]+=CASE;
    break;
  case GLUT_KEY_DOWN:
    g_position[1]-=CASE;
    break;
  case GLUT_KEY_RIGHT:
    g_position[0]+=CASE;
    break;
  case GLUT_KEY_LEFT:
    g_position[0]-=CASE;
    break;
  }
  drawScene();
}

void drawScene() {
  static bool first = true;
  if (first)
    {
      first = false;
      reset();
      return ;
    }
  COLOR_INC;
  COLOR_CHKMAX;
  COLOR_CHKMIN;
  if (g_position[1] > 1.2)
    g_position[1] = 1.2;
  else if (g_position[1] < -1.3)
    g_position[1] = -1.3;
  if (g_position[0] > 1.5)
    g_position[0] = 1.5;
  else if (g_position[0] < -1.7)
    g_position[0] = -1.7;

  glLoadIdentity(); //Reset the drawing perspective
  Cube cube(CASE, RED, GREEN, BLUE);
  cube.move(g_position[0], g_position[1], g_position[2]);
  cube.display(true);
  // create_cube(g_color[0], g_color[1], g_color[2] , CASE);
  // glutSwapBuffers(); //Send the 3D scene to the screen
}

int	main(int ac, char **av) {
  glutInit(&ac, av);
  glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
  glutInitWindowSize(WIDTH, HEIGTH);
  g_id = glutCreateWindow("Snake");
  glutMouseFunc(&handleMouse);
  glutMotionFunc(&handleMotion);
  glutKeyboardFunc(&handleKeypress);
  glutSpecialFunc(&handleSpecialKeypress);
  glutReshapeFunc(&handleResize);
  glutDisplayFunc(&drawScene);
  glutMainLoop();
  return 0;
}
