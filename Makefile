RM	=	rm -f

CFLAGS  +=	-lGL -lGLU -lglut -IGL/ -L.

NAME	=	CPaint

SRCS	=	main.cpp \
		Cube.cpp

OBJS	=	$(SRCS:.cpp=.o)

CXX	=	clang++

all:		$(NAME)

$(NAME):	$(OBJS)
		$(CXX) $(OBJS) $(CFLAGS) -o $(NAME)

clean:
		$(RM) $(OBJS)

fclean:		clean
		$(RM) $(NAME)

re:		fclean all

.PHONY:		all clean fclean re
