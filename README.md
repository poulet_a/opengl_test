# TODO
- Touches __6 RB__, __7 RV__, __8 BV__
- Touche __effacer__
- Encapsuler le code

# OPTIONS
## KEYS
- __UP__, __DOWN__, __LEFT__, __RIGTH__
- __x__ : reset the map
- __r__ : reset the option to default
- __t__ : reset all the colors
- __1__ : set option as _color degradation speed_
- __2__ : set option as _pen size_
- __3__ : change the _direction_ of the _color degradation_
- __4__ : set option as _color red_
- __5__ : set option as _color green_
- __6__ : set option as _color blue_
